import React, { Component } from 'react'
import { Link } from 'react-router-dom'

const perPage = 5
class AvbPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            query: '',
            value: '',
            limit: perPage,
            skip: 0,
            total: 0,
            avbs: []
        }
    }

    handlePageClickPrev = () => {
        let skip = this.state.skip - perPage
        if (skip < 0) return
        this.setState({ skip: skip })
    }

    handlePageClickNext = () => {
        let skip = this.state.skip + perPage
        this.setState({ skip: skip })
    }

    updateQuery = (event) => {
        this.setState({ value: event.target.value })
    }

    handlePageClickSearch = () => {
        this.setState({
            query: `&query=${this.state.value}`,
            skip: 0
        })
    }

    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.skip !== this.state.skip ||
            prevState.query !== this.state.query) {
            this.loadData();
        }
    }

    loadData() {
        const { limit, skip, query } = this.state
        fetch(`http://localhost:8081/?limit=${limit}&skip=${skip}${query}`)
            .then(res => res.json())
            .then((data) => {
                this.setState({ total: data.results.length })
                if (this.state.total == 0) {
                    let skip = this.state.skip - perPage
                    this.setState({ skip: skip })
                } else
                    this.setState({ avbs: data.results })
            })
    }

    render() {
        return (
            <div>
                <h1>Avb Folders</h1>
                <button onClick={this.handlePageClickSearch}>Search</button>
                <input type="text" onChange={this.updateQuery}></input>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Path</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.avbs.map((avb, index) => (
                            <tr key={index}>
                                <td>{avb.id}</td>
                                <td><Link to={`/${encodeURIComponent(avb.path)}`}>{avb.path}</Link></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button onClick={this.handlePageClickPrev}>Prev</button>
                <button onClick={this.handlePageClickNext}>Next</button>
            </div>
        );
    }
}

export default AvbPage;