import React, { Component } from 'react'

const perPage = 1
class Asset extends Component {

    constructor(props) {
        super(props)
        this.state = {
            folderId: '',
            type: '',
            value: '',
            limit: perPage,
            skip: 0,
            total: 0,
            assets: []
        }
    }

    handlePageClickPrev = () => {
        let skip = this.state.skip - perPage
        if (skip < 0) return
        this.setState({ skip: skip })
    }

    handlePageClickNext = () => {
        let skip = this.state.skip + perPage
        this.setState({ skip: skip })
    }

    updateQuery = (event) => {
        this.setState({ value: event.currentTarget.value })
    }

    handlePageClickSearch = () => {
        this.setState({
            skip: 0,
            type: `&type=${this.state.value}`
        })
    }

    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.skip !== this.state.skip ||
            prevState.type !== this.state.type) {
            this.loadData();
        }
    }

    loadData() {
        const { id } = this.props.match.params
        const { limit, skip, type } = this.state
        fetch(`http://localhost:8081/${encodeURIComponent(id)}?limit=${limit}&skip=${skip}${type}`)
            .then(resp => resp.json())
            .then((data) => {
                this.setState({ total: data.assets.length })
                if (this.state.total == 0) {
                    let skip = this.state.skip - perPage
                    this.setState({ skip: skip })
                } else
                    this.setState({
                        folderId: data.id,
                        assets: data.assets
                    })
            })
    }

    render() {
        return (
            <div>
                <h1>Content of folder with ID: {this.state.folderId}</h1>
                <button onClick={this.handlePageClickSearch}>Search by type</button>
                <input type="radio" name="type" value="filemob" onChange={this.updateQuery}></input>filemob
                <input type="radio" name="type" value="masterclip" onChange={this.updateQuery}></input>masterclip
                <input type="radio" name="type" value="subclip" onChange={this.updateQuery}></input>subclip
                <input type="radio" name="type" value="sequence" onChange={this.updateQuery}></input>sequence
                <input type="radio" name="type" value="group" onChange={this.updateQuery}></input>group
                {this.state.assets.map((asset, index) => (
                    <ul key={index}>
                        <h3>Mob ID:</h3>
                        <li>{asset.mobId}</li>
                        <h3>Base:</h3>
                        <li><strong>ID:</strong> {asset.base.id}</li>
                        <li><strong>Type:</strong> {asset.base.type}</li>
                        <h3>Common:</h3>
                        <li><strong>Name:</strong> {asset.common.name}</li>
                        <li><strong>Created:</strong> {asset.common.created}</li>
                        <li><strong>Modified:</strong> {asset.common.modified}</li>
                        <h3>Attributes:</h3>
                        {asset.attributes.map((attribute, index) => (
                            <ul key={index}>
                                <li><strong>Group: </strong>{attribute.group}</li>
                                <li><strong>Name: </strong>{attribute.name}</li>
                                <li><strong>Value: </strong>{attribute.value}</li>
                                <br></br>
                            </ul>
                        ))}
                    </ul>
                ))}
                <button onClick={this.handlePageClickPrev}>Prev</button>
                <button onClick={this.handlePageClickNext}>Next</button>
            </div>
        )
    }
}

export default Asset