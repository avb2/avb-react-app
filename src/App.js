import React from 'react'
import {Route, Switch} from 'react-router-dom'
import AvbPage from './pages/AvbPage'
import AssetPage from './pages/AssetPage'

export default function App() {
  return (
    <Switch>
      <Route exact path="/" component={AvbPage} />
      <Route path="/:id" component={AssetPage} />
    </Switch>
  );
}
